﻿using System.Web;
using System.Web.Optimization;

namespace MyrleaTechnology_WebSite
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            //bootsap , jquery , jquery val ,  modernizr , respond scripts :
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));



            //************ site template scripts  ********************* 


            bundles.Add(new ScriptBundle("~/bundles/global").Include("~/Scripts/AppScripts/GlobalFunctions.js"));


            bundles.Add(new ScriptBundle("~/bundles/lib").Include(
                    "~/Scripts/AppScripts/main.js",
                    "~/Content/BizPage/lib/easing/easing.min.js",
                    "~/Content/BizPage/lib/superfish/hoverIntent.js",
                    "~/Content/BizPage/lib/superfish/superfish.min.js",
                    "~/Content/BizPage/lib/wow/wow.min.js",
                    "~/Content/BizPage/lib/waypoints/waypoints.min.js",
                    "~/Content/BizPage/lib/counterup/counterup.min.js",
                    "~/Content/BizPage/lib/owlcarousel/owl.carousel.min.js",
                    "~/Content/BizPage/lib/isotope/isotope.pkgd.min.js",
                    "~/Content/BizPage/lib/lightbox/js/lightbox.min.js",
                    "~/Content/BizPage/lib/touchSwipe/jquery.touchSwipe.min.js",
                    "~/Content/BizPage/contactform/contactform.js"

                    ));




            //styles : 

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/BizPage/css/AdmLTE.css",
                      "~/Content/BizPage/css/style.css",
                      "~/Content/BizPage/lib/animate/animate.min.css",
                      "~/Content/BizPage/lib/owlcarousel/assets/owl.carousel.min.css",
                      "~/Content/BizPage/lib/lightbox/css/lightbox.min.css"

                      ).Include("~/Content/BizPage/lib/font-awesome/css/font-awesome.min.css", new CssRewriteUrlTransform())
                      .Include("~/Content/BizPage/lib/ionicons/css/ionicons.min.css", new CssRewriteUrlTransform())
                      );
        }
    }
}
