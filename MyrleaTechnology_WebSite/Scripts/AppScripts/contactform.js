jQuery(document).ready(function ($) {


	//Contact
	$('form.contactForm').submit(function () {


		//first check if the form is valid ? 
		var isvalid = $(this).valid();  // Tells whether the form is valid

		if (!isvalid) {

			alert("not valid !");
			return;
		}

		var str = $(this).serialize();

		var modalwait = null;

		$.ajax({
			type: "POST",
			url: "/CustomerMessages/Create",
			data: str,
			beforeSend: function () {

				modalwait = ShowWait("Please wait , your message is being sent...");
			},
			success: function (msg) {
				// alert(msg);
				if (msg == 'OK') {

					modalwait.modal('hide');

					modalinfo = ShowInfo("Your message has been sent successfully.", false);
					modalinfo.on('hidden.bs.modal', function () {
						location.reload();

					}); 
				}
				else {
					modalwait.modal('hide');
					ShowError(msg);
				}

			},
			error: function (err) {

				modalwait.modal('hide');
				ShowError(err);

			}

		});
		return false;
	});

});
