﻿//override the validator date function :
//!!!!!!! if we dont use this , the date input validator fails even in truly formatted inputs
$(function () {
	$.validator.methods.date = function (value, element) {
		

		var dateshort = this.optional(element) || moment(value, "DD.MM.YYYY", true).isValid();
		var datelong = this.optional(element) || moment(value, "DD.MM.YYYY HH:mm", true).isValid();
		var datelongest = this.optional(element) || moment(value, "DD.MM.YYYY HH:mm:ss", true).isValid();

		return dateshort || datelong || datelongest;


	}
});


/// Converts turish date string t o a date object 
$.fn.extend({

	toDate: function () {

		try {

			
			var tar = $(this).val(); // '16.01.2018 18:30:45'
			//
			var parts = tar.split(" "); //'16.01.2018' '18:30:45'

			var TarParts = parts[0].split(".");
			var day = TarParts[0];
			var month = TarParts[1];
			var year = TarParts[2];


			var TimeParts = parts[1].split(":");
			var hour = TimeParts[0];
			var minute = TimeParts[1];
			var second = TimeParts[2];

			var Tarih = new Date(year, month - 1, day, hour, minute, second);

			return Tarih;

		} catch (e) {

		}

	}

});



/// Converts turish date string t o a date object 
$.fn.extend({

	toShortDate: function () {

		try {


			var tar =  $(this).val(); // '16.01.2018 18:30:45'
			//
			var parts = tar.split(" "); //tar.indexOf(" ") >= 0 ? tar.split(" ") : tar.split(""); //'16.01.2018' '18:30:45'

			var TarParts = parts[0].split(".");
			var day = TarParts[0];
			var month = TarParts[1];
			var year = TarParts[2];

 

			var Tarih = new Date(year, month - 1, day);

			return Tarih;

		} catch (e) {

		}

	}

});

 

/*
    CreateDataTable function applies the DataTable plugin to given Html Div. Refer to https://datatables.net/ for all documentation.
    We use the Adimn-LTE Theme for this project and it uses DataTable plugin also , so that means we should use this plugin in alot of page.
    It's the best way to write a gloabal function and call it with only necessary parameters for individual DataGrids and avoding code repeating by this way.
    So here it is : thisd fucntion sets the language options , enables paging , searching , ordering etc..

    this function returns a DataGrid object which can be used fo event handling :
    var mygrid = CreateDataTable("records");
        mygrid.on( 'draw', function () {..... }); //table has completed event
        mygrid.on( 'page', function () {..... }); // page change event
        .... refer to https://datatables.net/reference/event/ for event list
    

    edit : here is a great link for using datatabel in serverside mode :https://echosteg.com/jquery-datatables-asp.net-mvc5-server-side

*/
function CreateDataTable(options) {


	options = $.extend({
		GridId: "", //Required ! the  Div Id of main container ( just put a div with this ID on to page ) 
		CreateButtonClick: function () { },
		EditButtonClick: function (btn, rowpk) { }, //edit button must be inserted in to html table before this function , data-grid attr must be equal to GridID and data-gridbutton='edit' + data-rowpk='givenrowID'!
		DeleteButtonClick: function (btn, rowpk) { }, //delete button must be inserted in to html table before this function , data-grid attr must be equal to GridID and data-gridbutton='delete'  + data-rowpk='givenrowID'!
		CustomButtonsClick: function (btn, rowpk) { },//custom buttons must be inserted in to html table before this function , data-grid attr must be equal to GridID and data-gridbutton='custom' + data-rowpk='givenrowID' !
		RowSelect: function (table, rowdata, rowdataarray, type, indexes) { },
		columnDefsArr: [],
		select: true,
		order: [[0, 'asc']], //[[0, 'asc'], [1, 'asc']]
		paging: true,
		processing: true,
		searching: true,
		ordering: true,
		info: true,

	}, options)

    /*datagrid plugin needs to find "thead" element in first row , if it can't it throws an error

        Unhandled exception at line 90, column 407 in http://localhost:53247/Content/Theme/plugins/datatables/jquery.dataTables.min.js
        0x800a138f - JavaScript çalışma zamanı hatası: Tanımsız veya boş referansın 'mData' özelliği alınamıyor occurred

        but most of the time we havent got "thead" in our view table ( because it is scaffolded : genereated automatic ! ) 
        so to solve this just append theads by a line of code : ( or write manule thead element into table )
    */

	$("#" + options.GridId).prepend($("<thead></thead>").append($('#' + options.GridId).find("tr:first")))

	//if we have edit/delete/custom buttons ( crud buttons + custom buttons) on the rows , attach their click events : ( before3 grid init !)
	//data_grid = "UsersDataGrid", data_gridbutton = "edit", data_rowpk

	$("a[data-grid='" + options.GridId + "'][data-gridbutton='edit'").click(function () {

		var btn = $(this);
		var rowpk = $(this).data("rowpk");
		options.EditButtonClick(btn, rowpk);
		// alert("edit ID:" + $(this).data("rowpk"));
	})

	$("a[data-grid='" + options.GridId + "'][data-gridbutton='delete'").click(function () {

		var btn = $(this);
		var rowpk = $(this).data("rowpk");
		options.DeleteButtonClick(btn, rowpk);
		//alert("delete ID:" + $(this).data("rowpk"));
	})


	$("a[data-grid='" + options.GridId + "'][data-gridbutton='custom'").click(function () {

		//there may be more them one custom button , this event is not an individual event !  
		var btn = $(this);
		var rowpk = $(this).data("rowpk");
		options.CustomButtonsClick(btn, rowpk);
		//alert("delete ID:" + $(this).data("rowpk"));
	})

	//get the count of edit,delete and custom buttons on a row to calculate the width of last column ( buttons column )
	//to do this : get  a[data-grid='grdiid'] elements in the last td of first tr in tbody :
	var buttons_on_a_row = $("#" + options.GridId + " tbody tr:first td:last a[data-grid='" + options.GridId + "']").length;


	if (options.columnDefsArr == undefined) {
		options.columnDefsArr = [];
	}
	options.columnDefsArr.push({ "width": ((buttons_on_a_row * 22) + 5) + "px", "targets": -1 });


	//before create datatable register date format with this plugin to order correctly
	$.fn.dataTable.moment('DD.MM.YYYY');
	//$.fn.dataTable.moment('dddd, MMMM Do, YYYY');


	var DataGrid = $('#' + options.GridId).DataTable({
		paging: options.paging,
		processing: options.processing,
		lengthChange: false,
		searching: options.searching,
		ordering: options.ordering,
		order: options.order,
		info: options.info,
		autoWidth: false,
		// scrollY: "600px", dont use , becaouse responsive fails about theads
		scrollX: true,
		//scrollCollapse: true,
		columnDefs: options.columnDefsArr,
		lengthMenu: [
			[10, 25, 50, -1],
			['10 Kayıt Göster', '25  Kayıt Göster', '50  Kayıt Göster', 'Tümünü Göster']
		],

		buttons: [
            /*  {
                  extend: 'pageLength',
                  className: 'btn-info',
              },
          */
			{
				extend: 'collection',
				text: '<span class="glyphicon glyphicon-th"></span>',
				className: 'btn-default',
				buttons: ['columnsToggle']
			},/*
            {
                extend: 'collection',
                text: 'Select',
                buttons : ['selected', 'selectedSingle', 'selectAll', 'selectNone','selectRows','selectColumns','selectCells'] 
            },*/
			{
				extend: 'collection',
				text: '<span class="glyphicon glyphicon-export"></span>',
				className: 'btn-default',
				buttons: ['copy', 'excel', 'csv', 'pdf']
			},
			{
				extend: 'print',
				text: '<span class="glyphicon glyphicon-print"></span>',
				className: 'btn-default'

			},

			{
				text: '<span class="glyphicon glyphicon-plus"></span>',
				className: 'btn-default',
				action: options.CreateButtonClick
                /*function (e, dt, node, config) {
                    alert('Button activated');
                }*/
			}
		],
		select: options.select,

		language: {
            /*
                for internationalization of datateble refer to    https://datatables.net/reference/option/language and  https://datatables.net/reference/option/
            */

			info: "Toplam _TOTAL_ kayıttan _START_ - _END_ arası gösteriliyor ",
			infoEmpty: "Listede kayıt bulunmuyor",
			infoFiltered: "( _MAX_ toplam kayıttan filtrelendi)",
			infoPostFix: "",
			lengthMenu: "Bir Sayfada _MENU_ kayıt göster",
			loadingRecords: "Yükleniyor...",
			processing: "Veriler alınıyor...",
			search: "Ara&nbsp;:",
			zeroRecords: "Eşleşen kayıt yok",
			emptyTable: "Listede kayıt yok",

			paginate:
				{
					first: "<<",
					previous: "<",
					next: ">",
					last: ">>"
				},

			aria: {
				sortAscending: ": Artan (A-Z) sıralama için aktifleştir.",
				sortDescending: ": Azalan(Z-A) sıralama için aktifleştir."
			}
		}



	});

	DataGrid.on('select', function (e, dt, type, indexes) {
        /*
        e: jQuery event object
        dt:DataTables API instance
        type: Items being selected. This can be row, column or cell.
        indexes : The DataTables' indexes of the selected items.
                    This can be used with the table selector methods (rows() for example).
                    For further information about the item indexes used by DataTables, please refer to row().index(), column().index() and cell().index() as appropriate.

        if (type === 'row') {
            var data = table.rows(indexes).data().pluck('id');
            var rowData = table.rows( indexes ).data().toArray();
        }

        */

		var rowdata = DataGrid.rows(indexes).data();
		var rowdataarray = DataGrid.rows(indexes).data().toArray();

		//pass necesseray prms :
		options.RowSelect(DataGrid, rowdata, rowdataarray, type, indexes);

	});

	//To show the buttons , we can use 2 ways , first dom prop : dom: 'Blfrtip' , or append them to a specific container : 
	DataGrid.buttons().container().appendTo($('.col-sm-6:eq(0)', DataGrid.table().container()));



	return DataGrid;

}


/*
    following three functions are written for geting the gelocation of user signed in.
    getLocation , showPosition, showError
*/

function getLocation(target, size) {

	if (navigator.geolocation) {

		navigator.geolocation.getCurrentPosition(function showPosition(position) {

			var latlon = position.coords.latitude + "," + position.coords.longitude;
			var img_url = "https://maps.googleapis.com/maps/api/staticmap?center="
				+ latlon + "&zoom=14&size=" + size + "&key=AIzaSyB_gS2CFrPxltndYSVi3woaoQCTciSJu7Q";
			target.html("<img src='" + img_url + "'>");

		},
			function showLocationError(error) {



				var err = "";
				switch (error.code) {
					case error.PERMISSION_DENIED:
						err = "Konum belirleme kullanıcı tarafından reddedildi !"
						break;
					case error.POSITION_UNAVAILABLE:
						err = "Konum bilgisine ulaşılamıyor !"
						break;
					case error.TIMEOUT:
						err = "Konum isteği zaman aşımına uğradı !"
						break;
					case error.UNKNOWN_ERROR:
						err = "Bilinmeyen Hata !"
						break;
				}

				target.html(err);
			});

	}
	else {

		target.html("Kullandığınız browser konum saptamayı desteklemiyor !");
	}
}




//----------------------------------------------------------------------------------------------------



/*

    ShowModal function creates some necessary  dynamical html ( divs for bootstrap modal) , append it to body ,
    apply given parameters , title , size , 

 //sanal divi ekle
    var divID = 'divModal_' + Create_RandomString(8);
    // $('body').remove(divID);
    $('body').append('<div style="display:none;" id="' + divID + '"  divType="BDY_ModalDiv" title="' + obj.DialogTitle + '" ></div>');


*/
function ShowModal(options) {


	var x = "";

	//merge options with default values object ( modify first object mode )  : 
	options = $.extend(true, {


		title: "Modal Title",
		URL: "",// sample : '@Url.Action("ActiontoGET", "Controllername", new { routeval1 = 1 })',
		staticHTML: "", // if this is given then this will be injected into modal body
		size: "",//modal-md , modal-lg , modal-sm , modal-xs
		top: "", // 5% / 10px etc.. 
		left: "",// 50% / 10px etc.. 
		width: "",
		height: "",
		fontsize: "",
		modalStyle: "primary", // primary :  bg-light-blue-active", info : bg-aqua-active  , success : bg-green-active  ,warning :  bg-yellow-active ,danger : bg-red-active  
		backdrop: 'static', // this means user can't close by clicking outside the modal
		keyboard: true, //the modal can be closed with Esc
		show: true,// show modal immediately when created
		manuelClose: true,
		hideclosebutton: false,
		buttons: [
            /* {
                 text: "Button",
                 class: "btn btn-success ",
                 onclick: function () {
                     alert('clicked me');
                 }
              }*/


		],

		onShow: function () { },
		onHide: function () { },
		onAjaxComplete: function () { },

	}, options);


	var headerbg = "bg-light-blue-active";

	switch (options.modalStyle) {
		case "Primary":
			headerbg = "bg-light-blue-active";
			break;
		case "info":
			headerbg = "bg-aqua-active";
			break;
		case "success":
			headerbg = "bg-green-active";
			break;
		case "warning":
			headerbg = "bg-yellow-active";
			break;
		case "danger":
			headerbg = "bg-red-active";
			break;
		default:
	}


	var rnd = randomString(6);
	var modalID = 'Modal_' + rnd;
	var modalBodyID = 'ModalBody_' + rnd;


	//add custom positioning 

	var styles = [];

	if (options.top != "") {
		styles.push('top: ' + options.top + ';');
	}
	if (options.left != "") {
		styles.push('left: ' + options.left + ';');
	}
	if (options.width != "") {
		styles.push('width: ' + options.width + ';');
	}
	if (options.height != "") {
		styles.push('height: ' + options.height + ';');
	}
	if (options.fontsize != "") {
		styles.push('font-size: ' + options.fontsize + ';');
	}



	var style = "";
	$.each(styles, function (idx) {
		style += styles[idx];
	});

	var modal = "".error
		+ '<div class="modal fade" id="' + modalID + '" role="dialog">'
		+ ' <div class="modal-dialog ' + options.size + '" style="' + style + '">'
		+ '     <div class="modal-content">'
		+ '         <div class="modal-header ' + headerbg + '">'
		+ '             <h4 class="modal-title">' + options.title + '</h4>'
		+ '  <button type="button" class="close" data-dismiss=' + (options.manuelClose ? '"modal"' : '') + '>&times;</button>'
		+ '         </div>'
		+ '         <div class="modal-body" id="' + modalBodyID + '">'
		+ ''
		+ '          </div>'
		+ '         <div class="modal-footer">'
		+ '<button type="btn btndanger" class="btn btn-default " data-dismiss=' + (options.manuelClose ? '"modal"' : '') + ' ' + (options.hideclosebutton ? 'style="display:none"' : "") + '>Kapat</button>'
		+ '             @buttons@'
		+ '         </div>'
		+ '      </div>'
		+ ' </div>'
		+ '</div>';


	//add given buttons to footer
	var buttonHtml = "";
	$.each(options.buttons, function (index) {

		var btnid = modalID + '_btn' + index;
		buttonHtml += ' <button type="button" class="' + this.class + '" modalId="' + modalID + '" modalBodyID ="' + modalBodyID + '"  id="' + btnid + '" >' + this.text + '</button>';

	});

	modal = modal.replace('@buttons@', buttonHtml);

	//add modal html to body
	$('body').append(modal);

	//assign the button click events given by user
	$.each(options.buttons, function (index) {

		var btnid = modalID + '_btn' + index;
		$('.modal-footer').on("click", "#" + btnid, this.onclick);


	});

	//get the modal body html : 
	if (options.URL != "" && options.URL != "undefined" && options.URL != undefined && options.URL != null) {
		$.ajax({
			url: options.URL,// sample : '@Url.Action("ActiontoGET", "Controllername", new { routeval1 = 1 })',
			type: 'GET',
			async: true,
			success: function (responseHTML) {
				$('#' + modalBodyID).html(responseHTML);
				options.onAjaxComplete();
			},
			error: function (err) {

				ShowError(err);

				//$('#' + modalBodyID).append("<b><h3>Hata ! - " + err.statusCode + " - </h3><p style='color:red;'>" + err.statusText + "<p></b>");

			}
		});
	}

	//set if statichtml exists , to body of modal:
	if (options.staticHTML != "") {

		$('#' + modalBodyID).append(options.staticHTML);
	}


	//Show the modal
	$('#' + modalID).modal({

		backdrop: options.backdrop, // this means user can't close by clicking outside the modal
		keyboard: options.keyboard, //the modal can be closed with Esc
		show: options.show

	}).on('shown.bs.modal', function () {

		//fire the onshwonevent callback

		options.onShow(modalID);

	}).on('hidden.bs.modal',
		function () {

			//remove the dynamic html from body 
			$('#' + modalID).remove();

			//fire the onshwonevent callback
			options.onHide();

		});


	return $('#' + modalID);


}

/*
    randomString function is used to creat uniqe random id values for dynamic html elements
*/
function randomString(len, charSet) {

	charSet = charSet || 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
	var randomString = '';

	for (var i = 0; i < len; i++) {
		var randomPoz = Math.floor(Math.random() * charSet.length);
		randomString += charSet.substring(randomPoz, randomPoz + 1);
	}

	return randomString;
}


/*
Showerror creates a small sized modal with 'danger' color header , and shows the error content

    in ajax use it like this :
        $.ajax({
                ....
                ......
                }
        error: function (jqXHR, textStatus, errorThrown) {

                ShowError(jqXHR.status, jqXHR.statusText);

        })

*/
function ShowError(err) {





	var modalerr = ShowModal({

		title: "Hata !",
		URL: '',
		fontsize: "14px",
		modalStyle: "danger",
		top: '15%',
		// width: '35%',
		staticHTML: ''
			+ '<div>'
			+ '	<div class="callout callout-danger">'
			+ '		<h4><i class="icon fa fa-warning" style="font-size:48px;"></i>  İşlem sırasında hata(lar) oluştu !</h4>'
			+ '<br/>'
			+ '		<p>Hata Açıklaması :  ' + err.statusText + '</p>'
			+ '<br/>'
			+ '	</div>'
			+ '<div class="box box-danger collapsed-box " id="boxdetail">'
			+ '		<div class="box-header with-border">'
			+ '		<h3 class="box-title">Hata Detayları</h3>'
			+ '			<div class="box-tools pull-right"><button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button></div>'
			+ '		</div>'
			+ '		<div class="box-body" style="height:200px;overflow:scroll;" >'
			+ '				<p>' + err.responseText + '</p>'
			+ '		</div>'
			+ '</div>'






		/*< div class="col-md-3" >
		<div class="box box-warning">
			<div class="box-header with-border">
				<h3 class="box-title">Collapsable</h3>

				<div class="box-tools pull-right"><button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button></div>
				<!-- /.box-tools -->
            </div>
			<!-- /.box-header -->
            <div class="box-body">
				The body of the box
            </div>
			<!-- /.box-body -->
          </div>
		<!-- /.box-- >
        </div >*/
	});

	return modalerr;


}

function ShowInfo(infotext, autoClose, closeCallBack) {

	var modalinfo = ShowModal({

		title: "Bilgi",
		URL: '',
		modalStyle: "success",
		top: '15%',
		width: '20%',
		staticHTML: ''
			+ '  <div>'
			+ '      <h3><i class="fa fa-info-circle text-green" ></i> Bilgi </h3>'
			+ '      <p><strong>' + infotext + '</strong></p>'
			+ '  </div>',
		onShow: function (modalId) {

			if (autoClose) {
				setTimeout(function () {
					$('#' + modalId).modal("hide");
				}, 3000);

			}


		},
		onHide: function () {
			if (closeCallBack) {
				closeCallBack();
			}
		}
	});

	return modalinfo;


}


function ShowWarning(infotext, autoClose, closeCallBack) {

	var modalinfo = ShowModal({

		title: "Bilgi",
		URL: '',
		modalStyle: "warning",
		top: '15%',
		width: '20%',
		staticHTML: ''
			+ '  <div>'
			+ '      <h3><i class="fa fa-info-circle text-yellow" ></i> Uyarı </h3>'
			+ '      <p><strong>' + infotext + '</strong></p>'
			+ '  </div>',
		onShow: function (modalId) {

			if (autoClose) {
				setTimeout(function () {
					$('#' + modalId).modal("hide");
				}, 3000);

			}


		},
		onHide: function () {
			if (closeCallBack) {
				closeCallBack();
			}
		}
	});

	return modalinfo;


}

function ShowWait(waitmsg) {

	modalwait = ShowModal({

		title: "Lütfen Bekleyin",
		URL: '',
		modalStyle: "warning",
		top: '15%',
		width: '20%',
		manuelClose: false,
		staticHTML: ''
			+ '  <div>'
			+ '      <h3>   <i class="fa fa-refresh fa-spin"></i>  Lütfen Bekleyin </h3>'
			+ '      <p>' + waitmsg + '</p>'
			+ '  </div>'
	});

	return modalwait;


}

function ShowConfirm(question, Callback_Yes, Callback_No) {

	var modalconfirm = ShowModal({

		title: "Onay",
		URL: '',
		modalStyle: "warning",
		top: '15%',
		width: '30%',
		staticHTML: ''
			+ '  <div>'
			+ '      <h3><i class="fa fa-question-circle text-yellow" ></i> Onay </h3>'
			+ '      <p style="padding-left:40px;">' + question + '</p>'
			+ '  </div>',
		hideclosebutton: true,
		buttons: [
			{
				text: "Evet",
				class: "btn btn-success ",
				onclick: function () {

					modalconfirm.modal('hide');
					if (Callback_Yes && typeof (Callback_Yes) == "function") {

						Callback_Yes();
					}
				}
			},
			{
				text: "Hayır",
				class: "btn btn-danger ",
				onclick: function () {

					modalconfirm.modal('hide');
					if (Callback_No && typeof (Callback_No) == "function") {
						Callback_No();
					}
				}
			}


		],
	});

	return modalconfirm;

}


//SET CURSOR POSITION
$.fn.setCursorPosition = function (pos) {

	this.each(function (index, elem) {

		if (elem.setSelectionRange) {
			elem.setSelectionRange(pos, pos);

		}
		else if (elem.createTextRange) {
			var range = elem.createTextRange();
			range.collapse(true);
			range.moveEnd('character', pos);
			range.moveStart('character', pos);
			range.select();
		}
	});
	return this;
};

